<?php
require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';
function jalbericht_config(){
    register_nav_menus(
        array(
            'jalberich_tema_main_menu' => 'jalberich_tema menú principal'
            
            
        )
    );

}
add_action( 'after_setup_theme', 'jalbericht_config', 0 );

function jalbericht_scripts(){
    wp_enqueue_script( "bootstrap_js", get_theme_file_uri("inc/js/bootstrap.min.js"), array("jquery"),"4.6", true );
    wp_enqueue_style( "bootstrap_css", get_theme_file_uri("inc/css/bootstrap.css"),array(), "4.6","all" );
    wp_enqueue_style( "home_css", get_theme_file_uri("css/home.css"),array(), "4.6","all" );

}
add_action( 'wp_enqueue_scripts', 'jalbericht_scripts');