<link rel="stylesheet" href="css/footer.css">
<footer>
<hr class="my-4">
<div class="pie">
Copyright© 2021     
</div>
<div class="redes">
<a href="https://www.instagram.com/lightgamemes/">
    <img src="<?=get_theme_file_uri("/img/instagram.png")?>" width="50" height="30" alt="">
</a>
<a href="https://twitter.com/LGamemes">
    <img src="<?=get_theme_file_uri("/img/twiter.png")?>" width="50" height="30" alt="">
</a>
<a href="https://www.facebook.com/lightgamemes.alberich">
    <img src="<?=get_theme_file_uri("/img/facebook.png")?>" width="40" height="40" alt="">
</a>

</div>
<div class="correo">
Contacta con nosotros: lightgamemes@gmail.com
</div>
</footer>

<?php wp_footer();?>
</body>

</head>
