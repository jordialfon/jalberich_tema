<?php
        get_header();
?>
<?php
        get_header();
?>
 <div id="carouselExampleCaptions" class="carousel slide " data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
          <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="<?=get_theme_file_uri("/img/xbox/xbox.png")?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
            <div class="text-dark">
            <h5>Xbox Series X</h5>
              <p>Ya ha llegado la nueva generación</p>
            </div>
            </div>
          </div>
          <div class="carousel-item">
            <img src="<?=get_theme_file_uri("/img/xbox/skyrim.jpg")?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h5>The Elder Scrolls V SKYRIM</h5>
              <p>Un explora un extendo mundo mediaval magico</p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="<?=get_theme_file_uri("/img/xbox/red2.png")?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h5>Red Dead Redemption 2</h5>
              <p>El salvaje oeste nunca estuvo tan vivo.</p>   
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</div></span>
        </a>
      </div>
      <div class="album py-5 bg-light">
        <div class="container">

        <div class="row">
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/xbox/brake.jpg")?>" width="100%" height="190" alt="">
                <div class="card-body">
                  <p class="card-text">Un popular juego con acción de tiempo amplificado y una trama llena de suspenso que cobra vida de forma nunca vista al fusionarse con una fascinante serie de acción. El tiempo mismo se fractura en un experimento con viajes temporales, Jack Joyce lucha contra la hermética Monarch Solutions para arreglar las cosas antes de que sea demasiado tarde. Las acciones que realices y las decisiones que tomes darán forma a tu experiencia en la serie, creando una experiencia de entretenimiento nueva y única.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/xbox/gear.jpg")?>" width="100%" height="190" alt="">
                <div class="card-body">
                  <p class="card-text">Con el conflicto bélico totalmente desencadenado, Kait Diaz se evade para averiguar su conexión con el enemigo y descubre el verdadero peligro para Sera: ella misma. Con nuevas funciones en el modo campaña, podrás llevar a tu personaje y diseños de armas a lo largo de todo el juego y disfrutar de niveles de dificultad y modificadores adicionales.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/xbox/halo.jpg")?>" width="100%" height="190" alt="">
                <div class="card-body">
                  <p class="card-text">Halo 5: Guardians ofrece experiencias multijugador épicas que incluyen distintos modos, completas herramientas de construcción de niveles y la historia más dramática de Halo hasta la fecha. ¡Con múltiples contenidos gratuitos desde el lanzamiento del juego, Halo 5: Guardians ofrece más contenido, más locura multijugador y más variedad que cualquier Halo!</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/xbox/division.jpg")?>" width="100%" height="190" alt="">
                <div class="card-body">                  
                  <p class="card-text">Washington D. C. está al borde del colapso. La anarquía y la inestabilidad amenazan nuestra sociedad, y los rumores sobre un golpe de estado en el Capitolio no hacen más que acrecentar el caos. Todos los agentes activos de la División necesitan desesperadamente salvar la ciudad a...</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/xbox/legion.jpeg")?>" width="100%" height="190" alt="">
                <div class="card-body">
                  <p class="card-text">Forma una resistencia con prácticamente cualquiera que te encuentres mientras hackeas, te infiltras y luchas para salvar Londres de la debacle de un futuro próximo. Bienvenidos a la Resistencia. Recluta y juega con cualquier personaje del juego. Cada personaje tiene su propia historia, personalidad y habilidades. Hackea drones armados, despliega robots araña y acaba con tus enemigos empleando una capa de ocultación de Realidad Aumentada.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/xbox/medium.jpg")?>" width="100%" height="200" alt="">
                <div class="card-body">
                  <p class="card-text">The Medium es un juego narrativo de terror psicológico en tercera persona con una innovadora jugabilidad basada en dos realidades. Utiliza habilidades psíquicas únicas reservadas para aquellos que tienen el don. Viaja entre las dos realidades o explora ambas a la vez. Usa la experiencia extracorporal para investigar lugares a los que tu yo del mundo real no puede ir. </p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/xbox/sea.jpg")?>" width="100%" height="225" alt="">
                <div class="card-body">
                  <p class="card-text">Explora un vasto mundo abierto repleto de islas vírgenes, navíos hundidos y misteriosos artefactos. Parte en pos de tesoros perdidos, enfréntate a capitanes esqueleto y custodia valiosos cargamentos de compañías comerciantes. Además, ¡puedes darte a la pesca y la caza o volcarte en los cientos de objetivos secundarios!</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/xbox/call.jpg")?>" width="100%" height="200" alt="">
                <div class="card-body">
                  <p class="card-text">Nada es lo que parece en la fascinante campaña para un jugador de Raven Software, donde te enfrentarás cara a cara a figuras históricas y verdades incómodas mientras luchas por todo el mundo en escenarios icónicos de la Guerra Fría como Berlín Este, Vietnam, la sede del KGB y más.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/xbox/llave.jpg")?>" width="100%" height="210" alt="">
                <div class="card-body">
                  <p class="card-text">KINGDOM HEARTS III es una historia de amistad en la que Sora y sus amigos se embarcan en una peligrosa aventura. Ambientado en una serie de mundos de Disney y Pixar, KINGDOM HEARTS narra el viaje de Sora, un joven que descubre inesperadamente que posee un poder espectacular. Con la ayuda de Donald y Goofy, Sora lucha para evitar que una fuerza maligna conocida como los sincorazón invada todo el universo.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    


<?php
        get_footer();
?>
