<?php
        get_header();
?>

      <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
    <div class="col-md-6 px-0">
      <h1 class="display-4 font-italic">Noticias de última hora</h1>
      <p class="lead my-3">En esta seccion lo que hacemos es dar informacion sobre todo relacionado con videoconsolas a tiempo real, si quieres saber todo a tiempo real este es tu sitio.</p>
      <a class="btn btn-primary btn-lg" href="#" role="button">Leer mas</a>
    </div>
  </div>

  <div class="row mb-2">
    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <strong class="d-inline-block mb-2 text-primary">Juegos</strong>
          <h3 class="mb-0">Ghost of Tushima</h3>
          <div class="mb-1 text-muted">28 Nov</div>
          <p class="card-text mb-auto">Este juego a sido considerado el mejor juego del año por si increible historia, sus personajes y su esilo de combate, si quieres saber mas informacion dale aqui abajo.</p>
          <a href="<?php echo site_url("playstation") ?>" class="stretched-link">Continuar leyendo</a>
        </div>
        <div class="col-auto d-none d-lg-block">
          <img src="<?=get_theme_file_uri("/img/fantasma.png")?>" width="250" height="250" alt="">
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <strong class="d-inline-block mb-2 text-primary">Juegos</strong>
          <h3 class="mb-0">Spider-man</h3>
          <div class="mb-1 text-muted">30 Nov</div>
          <p class="mb-auto">Este juego se ha vendido en cuestion de dias, por sus increibles graficos y su estilo de combate, ademas este juego tambien es compatible con la play 5, si quieres saber más informacion dale aqui abajo.</p>
          <a href="/playstation" class="stretched-link">Continuar leyendo</a>
        </div>
        <div class="col-auto d-none d-lg-block">
          <img src="<?=get_theme_file_uri("/img/spider.jpg")?>" width="200" height="250" alt="">
        </div>
      </div>
    </div>
  </div>
</div>

        <div class="entrada">
        <h2>Nueve países europeos se unen para investigar los problemas de los Joy-Con de Switch</h2>
          <p>28 de noviembre de 2020
            <p>Asociaciones de protección al consumidor de Bélgica, Francia, Grecia, Italia, Noruega, Portugal, Eslovenia y Eslovaquia comienzan una investigación conjunta sobre el 'drift' de los mandos de Switch.</p>
            <hr>
            <p>Las organizaciones de protección a los consumidores de nueve países europeos se han unido en una investigación conjunta en torno al drift de los Joy-Con, el problema que afecta a algunos jugadores de Nintendo Switch consistente en que las palancas de estos mandos reconocen movimiento aunque no se estén tocando, provocando que los personajes de los videojuegos se muevan solo o que navegar por un menú sea una tarea dificultosa.

              La Asociación del Consumidor holandesa (vía VGC) abrió un formulario web donde pedía a jugadores de Nintendo Switch que contaran cualquier problema que hubieran tenido con los mandos de la consola. La petición, según la mencionada organización, se hace en conjunto con organizaciones de protección al consumidor de Bélgica, Francia, Grecia, Italia, Noruega, Portugal, Eslovenia y Eslovaquia. A ellas hay que sumar la organización europea BEUC ("Bureau Européen des Unions de Consommateurs", por sus siglas en francés); en ella tienen representación los grupos españoles CECU (Confederación de consumidores y usuarios) y OCU (Organización de consumidores y usuarios), además de las organizaciones de otros 32 países de la Unión Europea y del Espacio Económico Europeo.</p>
        </div>

<?php
        get_footer();
?>