<?php
        get_header();
?>
<?php
        get_header();
?>
 <div id="carouselExampleCaptions" class="carousel slide " data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
          <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="<?=get_theme_file_uri("/img/nintendo/nintendoswitch329.png")?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
            <div class="ps5">
            </div>
              <h5>Nintendo Switch</h5>
              <p>La unica consola hibrida</p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="<?=get_theme_file_uri("/img/nintendo/zelda329.png")?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
            <h5>Zelda Breth of the wild</h5>
              <p>La ultima entrgada de una mitica saga, considerado uno de los mejores juegos de la historia</p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="<?=get_theme_file_uri("/img/nintendo/kart.png")?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
            <div class="text-dark">
            <h5>Mario Kart 8 Deluxe</h5>
              <p>Calienta motores y preparate para la version definitiva de Mario Kart 8</p>
              </div>

            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      <div class="album py-5 bg-light">
        <div class="container">

        <div class="row">
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/nintendo/3dworld.jpg")?>" width="100%" height="200" alt="">
                <div class="card-body">
                  <p class="card-text">¡Únete a Mario, Luigi, Peach y Toad en su misión para salvar el reino de las hadas en Super Mario 3D World + Bowser’s Fury para Nintendo Switch! Rescata a la princesa hada y a sus amigas, en solitario o con hasta 3 jugadores más, en esta versión mejorada de Super Mario 3D World.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/nintendo/dx.jpg")?>" width="100%" height="192" alt="">
                <div class="card-body">
                  <p class="card-text">Ayuda a Link a escapar de una tierra extraña en The Legend of Zelda: Link's Awakening para Nintendo Switch. Entabla amistad con un elenco de personajes de lo más variopinto, combate montones de monstruos y explora peligrosas mazmorras en esta aventura clásica con un nuevo estilo artístico.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/nintendo/maker.png")?>" width="100%" height="190" alt="">
                <div class="card-body">
                  <p class="card-text">¡Rompe las reglas y crea los niveles de Super Mario con los que siempre has soñado en Super Mario Maker 2, disponible en exclusiva para Nintendo Switch! Utiliza el amplio abanico de nuevas herramientas, funciones y elementos de niveles, dale rienda suelta a tu imaginación y crea niveles únicos que podrás compartir con amigos y jugadores de todo el mundo.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/nintendo/mario.jpg")?>" width="100%" height="190" alt="">
                <div class="card-body">
                  <p class="card-text">Mario regresa a los videojuegos con su primer gran título para Nintendo Switch. En esta ocasión lo hace con un juego en 3D de mundo abierto que recuerda en estética y jugabilidad a lo visto en Super Mario 64 o los juegos de la serie en Game Cube, pero que además incluye una gran cantidad de novedades, los mejores gráficos de la saga e interesantes mecánicas jugables como la gorra, la cual tendrá un gran protagonismo y ofrecerá diversas funcionalidades como ayudarnos a recorrer el escenario o la capacidad de controlar a los enemigos, obteniendo así increíbles y variadas habilidades.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/nintendo/pokemon.jpg")?>" width="100%" height="200" alt="">
                <div class="card-body">
                  <p class="card-text">Prepárate para descubrir Galar, una extensa región con lugares de lo más variado: desde apacibles campos hasta modernas ciudades, pasando por un frondoso bosque y montañas nevadas. Los habitantes y Pokémon colaboran estrechamente para impulsar las industrias de la región. Elige a tu compañero Pokémon para comenzar la aventura, y atrapa, entrena e intercambia Pokémon para crear tu propio equipo y así hacer frente al desafío de los gimnasios. ¡Si perseveras, podrás obtener el título de Campeón!</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/nintendo/zelda.jpg")?>" width="100%" height="200" alt="">
                <div class="card-body">
                  <p class="card-text">Se trata de la decimo-octava entrega de la saga Zelda, pero también es el primer juego de la popular serie para la consola de última generación de Nintendo. El título promete un estilo visual completamente nuevo con el que desmarcarse de todo lo visto hasta el momento con colores muy vivos y llamativos y unos gráficos que aprovechan a la perfección la técnica de Cel Shading. De igual forma su director Eiji Anouma prometió cambios sustanciales en lo que a jugabilidad se refiere.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/nintendo/smash.jpg")?>" width="100%" height="190" alt="">
                <div class="card-body">
                  <p class="card-text">Lanza a tus rivales del escenario usando uno de los nuevos personajes como Simon Belmont o King K. Rool que se unen a Inkling, Ridley y a todos los combatientes presentes en la historia de Super Smash Bros. Disfruta de la velocidad mejorada y combate en los nuevos escenarios basados en las series de Castlevania, Super Mario Odyssey y otras.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/nintendo/star.jpeg")?>" width="100%" height="200" alt="">
                <div class="card-body">
                  <p class="card-text">¡Descubre tres de las más grandes aventuras de Mario en 3D con Super Mario 3D All-Stars para Nintendo Switch! Este paquete especial incluye Super Mario 64, Super Mario Sunshine y Super Mario Galaxy. Todos los juegos se han puesto al día para Nintendo Switch y cuentan con gráficos en alta definición, controles adaptados a los mandos Joy-Con y un reproductor de música que incluye las épicas bandas sonoras de los tres juegos</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/nintendo/luigi.jpg")?>" width="100%" height="190" alt="">
                <div class="card-body">
                  <p class="card-text">Únete a Luigi, un héroe de lo más cobardica, en una aventura fantasmagórica (y un pelín viscosa, todo hay que decirlo) para salvar a Mario y compañía en Luigi's Mansion 3</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    


<?php
        get_footer();
?>


<?php
        get_footer();
?>