<?php
        get_header();
?>      
<div class="jumbotron">
        <h1 class="display-4">Hola, jugadores!</h1>
        <p class="lead">Esta es una página web para la venta de videojuegos, consolas y mucho más. También te contamos notícias relacionadas con el mundo del videojuego y reviews. Aquí puedes ver un montón de ofertas que seguro que te gustarán.</p>
        <hr class="my-4">
        <a class="btn btn-primary btn-lg" href="<?php echo site_url("noticias") ?>" role="button">Saber más >></a>
      </div>


        <div class="row">
            <div class="col-md-4 col-12">
                <div class="parrafo1">
                <div class="titulo">
                <a class="navbar-brand" id='logo' href="<?php echo site_url("nintendo")?>"><img class="logo" src="<?=get_theme_file_uri("img/logon.png") ?>" alt="Nintendo" width="200" height="65"></a>
            </div>
             <p>Nintendo es una empresa de entretenimiento dedicada a crear consolas y videojuegos. La compañia nipona es la más veterana que lleva desde el 84 con su NES(famicom). Su ultima consola es la Nintendo switch que aunque se queda muy corta de potencia comparado con su competencia, esta a diferencia de las demas es una consola hibrida (es de sobremesa y portatil a la vez), tambien tiene un modelo solo portatil llamado nintendo switch lite.</p>
         <div class="boton">
         <a class="btn btn-primary btn-lg" href="<?php echo site_url("nintendo") ?>" role="button">Detalles >></a>
         </div> 
                    </div>
                    </div>

         
            <div class="col-md-4 col-12">
            <div class="parrafo2">
                <div class="titulo">
                <a class="navbar-brand" id='logo' href="<?php echo site_url("playstation")?>"><img class="logo" src="<?=get_theme_file_uri("img/logop.png") ?>" alt="PlayStation" width="250" height="50"></a>
                </div>
            <p>Es el nombre de una serie de consolas de videojuegos creadas y desarrolladas por Sony Interactive Entertainment. Playstation desde sus inicios con la Playstatio 1 le ha ido muy bien, siendo la primera consola en llegar a las 100 millones de unidades. La ultima playstation que ha salido es la PlayStation 5 que viene con muchas mejoras, mas espacio, mejores graficos, tiempos de carga reducidos... El punto fuerte de esta consola son sus exclusivos.  </p>
            <div class="boton">
            <a class="btn btn-primary btn-lg" href="<?php echo site_url("playstation") ?>" role="button">Detalles >></a>
                      </div>
                </div>   
                </div>                
        
            <div class="col-md-4 col-12">
                <div class="parrafo3">
                    <div class="titulo">
                    <a class="navbar-brand" id='logo' href="<?php echo site_url("xbox")?>"><img class="logo" src="<?=get_theme_file_uri("img/logox.png") ?>" alt="Xbox" width="175" height="50"></a>
                </div>

            <p>Xbox es una marca de videojuegos creada por Microsoft que incluye una serie de videoconsolas desarrolladas por la misma compañía, así como juegos, servicios como Xbox Game Pass (que es la major baza de Xbox ya que con un reduido precio mensualmente tienes una gran cantidad de juegos) y línea Xbox Live. Su ultima consola la Xbox series x es la más potente de su generación con 12 Teraflops</p>
            <div class="boton">
            <a class="btn btn-primary btn-lg" href="<?php echo site_url("xbox") ?>" role="button">Detalles >></a>
             
                    </div>
                    </div>
</div>
</div>      

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
   </body>
</html>
            
   


</div>
    <?php
        get_footer();
?>