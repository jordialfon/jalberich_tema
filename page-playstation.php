<?php
        get_header();
?>
 <div id="carouselExampleCaptions" class="carousel slide " data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
          <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="<?=get_theme_file_uri("/img/playstation/ps5.png")?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
            <div class="ps5">
            </div>
            <div class="text-primary">
            <h5>PlayStation 5</h5>
              <p>Ya ha llegado la nueva generación</p>
            </div>
            </div>
          </div>
          <div class="carousel-item">
            <img src="<?=get_theme_file_uri("/img/playstation/tomb.png")?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h5>Shadow of the Tomb Raider</h5>
              <p>Que le deparará a Lara en el final de la nueva trilogía?</p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="<?=get_theme_file_uri("/img/playstation/ho.png")?>" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h5>Horizon Zero Down</h5>
              <p>Un increible mundo abierto de la mano de Guerrilla Games</p>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      <div class="album py-5 bg-light">
        <div class="container">

        <div class="row">
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/playstation/assasin.jpeg")?>" width="100%" height="200" alt="">
                <div class="card-body">
                  <p class="card-text">Assassin’s Creed Valhalla es la nueva entrega de Assassin’s Creed de Ubisoft, la saga de acción y aventuras en mundo abierto más famosa de la desarrolladora de videojuegos. En esta ocasión, y siguiendo con los toques RPG de las últimas entregas, viajaremos al siglo IX después de Cristo, llegando a una Gran Bretaña invadida por vikingos.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/playstation/ellie.jpg")?>" width="100%" height="225" alt="">
                <div class="card-body">
                  <p class="card-text">The Last of Us 2 es la secuela de The Last of Us, uno de los juegos más aclamados de la consola PS3, que apareció en 2015 en PS4 en versión remasterizada. Esta continuación ha sido anunciada en el evento PlayStation Experience de diciembre de 2016, generando grandísimas expectativas entre los jugadores.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/playstation/godofwar.jpg")?>" width="100%" height="200" alt="">
                <div class="card-body">
                  <p class="card-text">Esta nueva entrega para PlayStation 4, si bien mantendrá varios de los ingredientes indivisibles de su jugabilidad, apostará por un nuevo comienzo para el personaje y una ambientación nórdica, ofreciéndonos una perspectiva más madura y realista de la mitología de dioses y monstruos milenarios habitual en la serie de títulos. </p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/playstation/division.jpg")?>" width="100%" height="200" alt="">
                <div class="card-body">
                  <p class="card-text">Washington D. C. está al borde del colapso. La anarquía y la inestabilidad amenazan nuestra sociedad, y los rumores sobre un golpe de estado en el Capitolio no hacen más que acrecentar el caos. Todos los agentes activos de la División necesitan desesperadamente salvar la ciudad a...</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/playstation/miles.jpg")?>" width="100%" height="200" alt="">
                <div class="card-body">
                  <p class="card-text">Spider-Man: Miles Morales es el nuevo juego de acción y aventura en mundo abierto de Insomniac Games para PlayStation 5, que cuenta como título independiente de Marvel’s Spider-Man para PlayStation 4 aunque ofrece su misma fórmula jugable. De esta forma, encarnando a Morales y no a Peter Parker, viajaremos por una ciudad de Nueva York mejor recreada y detallada, combatiendo el crimen y luchando contra los habituales villanos de los cómics de Marvel.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/playstation/detroit.jpg")?>" width="100%" height="200" alt="">
                <div class="card-body">
                  <p class="card-text">Detroit, 2038. La tecnología ha evolucionado hasta tal punto de que hay androides similares a los humanos por todo el planeta. Hablan, se mueven y actúan como los humanos, pero no son más que máquinas a su servicio.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/playstation/fifa.jpeg")?>" width="100%" height="225" alt="">
                <div class="card-body">
                  <p class="card-text">FIFA 21 es la nueva entrega del simulador de fútbol de EA Sports, uno de los juegos deportivos más influyentes del mundo. La entrega mejorará gráficamente y contará con jugabilidad avanzada, como un posicionamiento de los jugadores más inteligente, tanto en defensa como en ataque, o un sistema actualizado de enfrentamientos uno contra uno con regates dedicados. </p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/playstation/ghost.jpg")?>" width="100%" height="200" alt="">
                <div class="card-body">
                  <p class="card-text">Sucker Punch, creadores de la saga inFamous presentan un nuevo videojuego de acción, sigilo y aventura para PlayStation 4 de forma exclusiva, que cambia la ambientación de ciencia ficción y personajes mutantes para trasladarnos a un pasado histórico de gran veracidad. Ghost of Tsushima nos llevará a un Japón feudal exquisitamente recreado en lo que es uno de los videojuegos más ambiciosos de la plataforma de Sony.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img src="<?=get_theme_file_uri("/img/playstation/nathan.jpg")?>" width="100%" height="200" alt="">
                <div class="card-body">
                  <p class="card-text">Uncharted 4: El Desenlace del Ladrón es la llegada de Nathan Drake a la nueva generación de videojuegos, en una cuarta parte de la serie desarrollada por Naughty Dog que nos llevará de nuevo a vivir aventuras en los lugares más recónditos del mundo.</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Ver</button>
                      <button type="button" class="btn btn-sm btn-outline-secondary">Comprar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    


<?php
        get_footer();
?>

